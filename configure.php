<?php

define('DB_DNS',      'mysql:dbname=forums;host=localhost;charset=utf8');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'qweasdzxc');

define('SESSION_TTL',     3600); // един час 
define('LAST_ACTIVITY',    900); // 15 минути

define('TOPICS_PER_PAGE',   30);
define('POSTS_PER_PAGE',    20);
define('MEMBERS_PER_PAGE',  20);
define('MESSAGES_PER_PAGE', 20);

//define('CLIENT_IP_ADDRESS', 'REMOTE_ADDR');
define('CLIENT_IP_ADDRESS', 'HTTP_X_FORWARDED_FOR');

define('FLOOD_LIMIT', 15); // 15 секунди

define('FORUMS_BASE_URL', 'http://bgdev.saas.bg/forums/');
define('LOGIN_URL',       FORUMS_BASE_URL . 'index.php?act=Login&CODE=01');
define('LOGOUT_URL',      FORUMS_BASE_URL . 'index.php?act=Login&CODE=03');
define('POST_URL',        FORUMS_BASE_URL . 'index.php');