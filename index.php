<?php
require_once './configure.php';

spl_autoload_register(function (string $class) {
	$class = str_replace('\\', '/', $class);
	require_once $class . '.php';
});

use \org\bgdev\forums\Application;
use \org\bgdev\forums\Response;
use \org\bgdev\forums\endpoints\Endpoint;

use \org\bgdev\forums\endpoints\StatusEndpoint;
use \org\bgdev\forums\endpoints\CategoriesEndpoint;
use \org\bgdev\forums\endpoints\CategoryEndpoint;
use \org\bgdev\forums\endpoints\ForumEndpoint;
use \org\bgdev\forums\endpoints\TopicEndpoint;
use \org\bgdev\forums\endpoints\MembersEndpoint;
use \org\bgdev\forums\endpoints\MemberEndpoint;
use \org\bgdev\forums\endpoints\MessagesEndpoint;
use \org\bgdev\forums\endpoints\MessageEndpoint;
use \org\bgdev\forums\endpoints\AuthKeyEndpoint;
use \org\bgdev\forums\endpoints\RedirectEndpoint;

use \org\bgdev\forums\filters\FloodFilter;

use \org\bgdev\forums\endpoints\LoginEndpoint;
use \org\bgdev\forums\endpoints\LogoutEndpoint;

set_exception_handler(function (\Throwable $e) {
	$error = [
		'message' => $e->getMessage(), 
		'code' => $e->getCode()
	];
	
	exit(new Response($error, 500));
});

$app = new Application();

$app->get("#^/status/?$#i", function (array $args): Endpoint {
	return new StatusEndpoint();
});

$app->get("#^/categories/?$#i", function (array $args): Endpoint {
	return new CategoriesEndpoint();
});

$app->get("#^/category/(?<category_id>\d+)/?$#i", function (array $args): Endpoint {
	return new CategoryEndpoint($args);
});

$app->get("#^/forum/(?<forum_id>\d+)(/page/(?<page>\d+))?/?$#i", function (array $args): Endpoint {
	return new ForumEndpoint($args);
});

$app->get("#^/forum/(?<forum_id>\d+)/offset/(?<offset>\d+)(/limit/(?<limit>\d+))?/?$#i", function (array $args): Endpoint {
	return new ForumEndpoint($args);
});

$app->get("#^/topic/(?<topic_id>\d+)(/entry/(?<entry>\d+))?(/page/(?<page>\d+))?/?$#i", function (array $args): Endpoint {
	return new TopicEndpoint($args);
});

$app->get("#^/topic/(?<topic_id>\d+)(/entry/(?<entry>\d+))?/offset/(?<offset>\d+)(/limit/(?<limit>\d+))?/?$#i", function (array $args): Endpoint {
	return new TopicEndpoint($args);
});

$app->get("#^/members(/page/(?<page>\d+))?/?$#i", function (array $args): Endpoint {
	return new MembersEndpoint($args);
});

$app->get("#^/members/offset/(?<offset>\d+)(/limit/(?<limit>\d+))?/?$#i", function (array $args): Endpoint {
	return new MembersEndpoint($args);
});

$app->get("#^/member/(?<member_id>\d+)/?$#i", function (array $args): Endpoint {
	return new MemberEndpoint($args);
});

$app->get("#^/messages/(inbox|outbox)(/page/(?<page>\d+))?/?$#i", function (array $args): Endpoint {
	return new MessagesEndpoint($args);
});

$app->get("#^/messages/(inbox|outbox)/offset/(?<offset>\d+)(/limit/(?<limit>\d+))?/?$#i", function (array $args): Endpoint {
	return new MessagesEndpoint($args);
});

$app->get("#^/message/(?<msg_id>\d+)/?$#i", function (array $args): Endpoint {
	return new MessageEndpoint($args);
});

$app->get("#^/authkey/?$#i", function (array $args): Endpoint {
	return new AuthKeyEndpoint($args);
});

$app->post("#^/login/?$#i", function(array $args): Endpoint {
	$endpint = new RedirectEndpoint($args);
	$endpint->setLocation(LOGIN_URL);
	
	return $endpint;
});

$app->get("#^/logout/?$#i", function(array $args): Endpoint {
	$endpint = new RedirectEndpoint($args);
	$endpint->setLocation(LOGOUT_URL);
	
	return $endpint;
});

$app->post("#^/post/?$#i", function(array $args): Endpoint {
	$endpint = new RedirectEndpoint($args);
	$endpint->setLocation(POST_URL);
	
	$endpint->addFilter(new FloodFilter());
	
	return $endpint;
});

$app->run();