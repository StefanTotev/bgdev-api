<?php
namespace org\bgdev\forums;

use \org\bgdev\forums\database\DBHelper;
use \org\bgdev\forums\database\SessionHelper;
use \org\bgdev\forums\database\StatusHelper;

class Application {
	private $map = [
		'GET' => [],
		'POST' => [],
		'PUT' => [],
		'DELETE' => []
	];
	
	private $method;
	private $path;

	public function __construct() {
		$this->method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
		$this->path = filter_input(INPUT_SERVER, 'QUERY_STRING');
	}
	
	private function map(string $method, string $pattern, callable $callback): void {
		$this->map[$method][$pattern] = $callback;
	}
	
	public function get(string $pattern, callable $callback): void {
		$this->map('GET', $pattern, $callback);
	}
	
	public function post(string $pattern, callable $callback): void {
		$this->map('POST', $pattern, $callback);
	}
	
	public function put(string $pattern, callable $callback): void {
		$this->map('PUT', $pattern, $callback);
	}
	
	public function delete(string $pattern, callable $callback): void {
		$this->map('DELETE', $pattern, $callback);
	}

	public function run(): void {
		foreach($this->map[$this->method] as $pattern => $callback) {
			
			$args = [];
			
			if(preg_match($pattern, $this->path, $args)) {
				$db = new DBHelper();
				
				$session = new SessionHelper($db);
				$session->start();
				
				$endpoint = $callback($args);
				
				$endpoint->setDBHelper($db);
				$endpoint->setSession($session);
				
				(new StatusHelper($endpoint))->updateLastActivity();
				$response = $endpoint->getResponse();
				
				$session->save();
				exit($response);
			}
		}
		
		throw new \Exception("Bad Request", 400);
	}
}
