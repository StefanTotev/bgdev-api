<?php

namespace org\bgdev\forums;

class Response {
	private $value;
	private $response_code;
	
	public function __construct($value, int $response_code = 200) {
		$this->value = $value;
		$this->response_code = $response_code;
	}

	public function __toString(): string {
		http_response_code($this->response_code);
		
		$origin = filter_input(INPUT_SERVER, 'HTTP_ORIGIN');
		if($origin != null) {
			header('Access-Control-Allow-Origin: ' . $origin);
			header('Access-Control-Allow-Credentials: true');
		}
		
		if($this->response_code >= 300 && $this->response_code < 400) {
			if(is_array($this->value) && array_key_exists('location', $this->value)) {
				header('Location: ' . $this->value['location']);
				return '';
			}
		}
		
		header('Content-Type: application/json; charset=utf-8');
		return json_encode($this->value, JSON_UNESCAPED_UNICODE);
	}
}
