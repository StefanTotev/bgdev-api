<?php
namespace org\bgdev\forums\database;

use \org\bgdev\forums\endpoints\Endpoint;

abstract class AbstractHelper {
	protected $endpoint;
	
	public function __construct(Endpoint $endpoint) {
		$this->endpoint = $endpoint;
	}
	
	protected function getDBHelper(): DBHelper {
		return $this->endpoint->getDBHelper();
	}

	protected function getSession(): SessionHelper {
		return $this->endpoint->getSession();
	}
}
