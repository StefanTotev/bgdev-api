<?php
namespace org\bgdev\forums\database;

class AuthKeyHelper extends AbstractHelper {
	
	function getAuthKey(): array {
		/*
		 * this is only here to prevent it breaking on guests
		 * md5 хеш на горния стринг се явява auth_key за Guest
		 */
		
		if($this->getSession()->getMemberId() == 0) {
			return [
				'auth_key' => md5('this is only here to prevent it breaking on guests')
			];
		}
		
		$query = 
				'select ' .
					'md5(concat(email, \'&\', password, \'&\', joined)) as auth_key ' .
				'from ibf_members ' .
				'where id = :member_id';
		
		return $this->getDBHelper()->fetch($query, [
			'member_id' => $this->getSession()->getMemberId()
		]);
	}
}
