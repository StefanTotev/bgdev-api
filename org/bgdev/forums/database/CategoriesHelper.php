<?php
namespace org\bgdev\forums\database;

class CategoriesHelper extends AbstractHelper {
	
	public function getCategory(int $id): array {
		$query = 
				'select ' .
					'id, name, description, image, url ' .
				'from ibf_categories ' .
				'where id = :id and state = 1 ' . // show only visible category
				'order by position asc';
		
		return $this->getDBHelper()->fetch($query, [
			'id' => $id
		]);
	}
	
	public function getAllCategories(): array {
		$query = 
				'select ' .
					'id, name, description, image, url ' .
				'from ibf_categories ' .
				'where state = 1 ' . // show only visible category
				'order by position asc';
		
		return $this->getDBHelper()->fetchAll($query);
	}
}
