<?php
namespace org\bgdev\forums\database;

class DBHelper {
	
	private $db;

	public function __construct() {
		$this->db = new \PDO(DB_DNS, DB_USERNAME, DB_PASSWORD);
		$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$this->db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
	}
	
	public function exists(string $sql, array $args = null): bool {
		$statement = $this->db->prepare($sql);
		if($statement->execute($args)) {
			return $statement->rowCount() > 0;
		}
		
		return false;
	}
	
	public function execute(string $sql, array $args = null): bool {
		$statement = $this->db->prepare($sql);
		return $statement->execute($args);
	}

	public function fetchAll(string $sql, array $args = null): array {
		$statement = $this->db->prepare($sql);
		$statement->execute($args);

		return $statement->fetchAll(\PDO::FETCH_ASSOC);
	}
	
	public function fetch(string $sql, array $args = null): array {
		$statement = $this->db->prepare($sql);
		$statement->execute($args);

		$res = $statement->fetch(\PDO::FETCH_ASSOC);
		
		if($res === false) {
			return [];
		}
		
		return $res;
	}
}
