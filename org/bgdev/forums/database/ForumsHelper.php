<?php
namespace org\bgdev\forums\database;

class ForumsHelper extends AbstractHelper {
	const ROOT_FORUM = -1;
	
	public function getForum(int $id): array {
		$query = 
				'select ' .
					//'* ' .
					'id, name, description, status, topics, posts, ' .
					'category as category_id, parent_id, ' .
					'last_id, last_title, last_poster_id, last_post, last_poster_name ' .
				'from ibf_forums ' .
				'where id = :id ' .
					'and (read_perms = \'*\' or read_perms like :group)';
		
		return $this->getDBHelper()->fetch($query, [
			'id' => $id,
			'group' => '%' . $this->getSession()->getMemberGroup() . '%'
		]);
	}

	public function getForums(int $category_id, int $parent_id = ForumsHelper::ROOT_FORUM): array {
		$query = 
				'select ' .
					//'* ' .
					'id, name, description, status, topics, posts, ' .
					'last_id, last_title, last_poster_id, last_post, last_poster_name ' .
				'from ibf_forums ' .
				'where category = :category_id and parent_id = :parent_id ' .
					'and (read_perms = \'*\' or read_perms like :group) ' .
				'order by position asc';
		
		return $this->getDBHelper()->fetchAll($query, [
			'category_id' => $category_id, 
			'parent_id' => $parent_id,
			'group' => '%' . $this->getSession()->getMemberGroup() . '%'
		]);
	}
	
	public function getSubForums(int $parent_id): array {
		$query = 
				'select ' .
					//'* ' .
					'id, name, description, status, topics, posts, ' .
					'last_id, last_title, last_poster_id, last_post, last_poster_name ' .
				'from ibf_forums ' .
				'where parent_id = :parent_id ' .
					'and (read_perms = \'*\' or read_perms like :group) ' .
				'order by position asc';
		
		return $this->getDBHelper()->fetchAll($query, [
			'parent_id' => $parent_id,
			'group' => '%' . $this->getSession()->getMemberGroup() . '%'
		]);
	}
}
