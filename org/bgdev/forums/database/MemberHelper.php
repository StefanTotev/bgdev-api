<?php
namespace org\bgdev\forums\database;

class MemberHelper extends AbstractHelper {
	
	public function isFlood(): bool {
		$query = 
				'select 1 from ibf_members ' .
				'where id = :id and (unix_timestamp() - ifnull(last_post, 0)) < :limit';
		
		return $this->getDBHelper()->exists($query, [
			'id'=> $this->getSession()->getMemberId(),
			'limit' => FLOOD_LIMIT
		]);
	}
	
	private function updateSession(array $member): void {
		if(empty($member)) {
			throw new \Exception('Forbidden', 403);
		}
		
		$this->getSession()->setMemberId((int)$member['id']);
		$this->getSession()->setMemberName($member['name']);
		$this->getSession()->setMemberGroup((int)$member['mgroup']);
	}

	public function login(array $args): array {
		$query =
				'select ' .
					'id, name, mgroup ' .
				'from ibf_members ' .
				'where name = :username and password = md5(:password) ' .
				'limit 1';
		
		$member = $this->getDBHelper()->fetch($query, $args);
		$this->updateSession($member);
		
		return [
			'status' => 'success'
		];
	}
	
	public function logout(): array {
		$query = 
				'select id, name, mgroup from ibf_members where id = 0';
		
		$guest = $this->getDBHelper()->fetch($query);
		$this->updateSession($guest);
		
		return [
			'status' => 'success'
		];
	}
}
