<?php
namespace org\bgdev\forums\database;

class SessionHelper {
	private $helper;

	private $session_id;
	private $session_data;
			
	function __construct(DBHelper $helper) {
		$this->helper = $helper;
	}
	
	private function loadSession(): array {
		$query = 'select * from ibf_sessions where id = :session_id';
		
		return $this->helper->fetch($query, [
			'session_id' => $this->session_id
		]);
	}
	
	private function createSession(): array {
		$query =
				'insert into ibf_sessions ' .
				'values (' .
					':id, ' .
					'(select name from ibf_members where id = 0), ' .
					'0, :ip_address, :browser, ' .
					'unix_timestamp(), 0, :location, ' .
					'(select mgroup from ibf_members where id = 0), ' .
					'0, 0' .
				')';
		
		//error_log(CLIENT_IP_ADDRESS . ': ' . filter_input(INPUT_SERVER, CLIENT_IP_ADDRESS));
		
		$this->helper->execute($query, [
			'id' => $this->session_id,
			'ip_address' => filter_input(INPUT_SERVER, CLIENT_IP_ADDRESS),
			'browser' => 'rest api',
			'location' => 'idx,,',
		]);
				
		return $this->loadSession();
	}
	
	private function deleteExpiredSession(): bool {
		$quey = "delete from ibf_sessions where unix_timestamp() - running_time > :ttl";
		
		return $this->helper->execute($quey, [
			'ttl' => (int)SESSION_TTL
		]);
	}

	public function save(): bool {
		$query =
				'update ibf_sessions set ' .
					'member_name = :member_name, ' .
					'member_id = :member_id, ' .
					'ip_address = :ip_address, ' .
					'browser = :browser, ' .
					'running_time = :running_time, ' .
					'login_type = :login_type, ' .
					'location = :location, ' .
					'member_group = :member_group, ' .
					'in_forum = :in_forum, ' .
					'in_topic = :in_topic ' .
				'where id = :id';
		
		//error_log(CLIENT_IP_ADDRESS . ': ' . filter_input(INPUT_SERVER, CLIENT_IP_ADDRESS));
		
		$this->session_data['ip_address'] = filter_input(INPUT_SERVER, CLIENT_IP_ADDRESS);
		$this->session_data['running_time'] = time();
		
		return $this->helper->execute($query, $this->session_data);
	}

	public function start(): void {
		$this->deleteExpiredSession();
		
		$this->session_id = array_key_exists('session_id', $_COOKIE) ? $_COOKIE['session_id'] : md5(uniqid(microtime()));
		$this->session_data = $this->loadSession();
		
		if(empty($this->session_data)) {
			$this->session_data = $this->createSession();
		}
		
		if(empty($this->session_data)) {
			throw new \Exception("Failed to start session", 500);
		}
		
		setcookie('session_id', $this->session_id, 0, '/', '', false, true);
	}
	
	private function getIntValue(string $key, int $def = 0): int {
		if(array_key_exists($key, $this->session_data)) {
			$value = $this->session_data[$key];
			
			return is_null($value) ? $def : (int)$value;
		}
		
		return $def;
	}

	public function getSessionId(): string {
		return $this->session_id;
	}

	public function getMemberId(): int {	
		return $this->getIntValue('member_id');
	}
	
	public function setMemberId(int $id): void {	
		$this->session_data['member_id'] = $id;
	}
	
	public function getMemberName(): string {	
		return (string)$this->session_data['member_name'];
	}
	
	public function setMemberName(string $name): void {	
		$this->session_data['member_name'] = $name;
	}
	
	public function getMemberGroup(): int {
		return $this->getIntValue('member_group');
	}
	
	public function setMemberGroup(int $group): void {
		$this->session_data['member_group'] = $group;
	}
	
	public function getCurrentForum(): int {
		return $this->getIntValue('in_forum');
	}
	
	public function setCurrentForum(int $id) {
		$this->session_data['in_forum'] = $id;
	}
	
	public function getCurrentTopic(): int {
		return $this->getIntValue('in_topic');
	}
	
	public function setCurrentTopic(int $id) {
		$this->session_data['in_topic'] = $id;
	}
}
