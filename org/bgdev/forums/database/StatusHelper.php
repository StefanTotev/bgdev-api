<?php
namespace org\bgdev\forums\database;

class StatusHelper extends AbstractHelper {
	
	public function updateLastActivity(): void {
		$query = 
				'update ibf_stats set ' .
					'most_count = (' .
						'select count(*) from ibf_sessions ' .
						'where unix_timestamp() - running_time < :last_activity_1 ' .
					'), ' .
					'most_date = unix_timestamp() ' .
				'where most_count < (' .
					'select count(*) from ibf_sessions ' .
					'where unix_timestamp() - running_time < :last_activity_2 ' .
				')';
		
		$this->getDBHelper()->execute($query, [
			'last_activity_1' => LAST_ACTIVITY, 
			'last_activity_2' => LAST_ACTIVITY
		]);
	}

	public function getSystemInfo(): array {
		$query =
				'select ' .
					'total_replies, ' .
					'total_topics, ' .
					'last_mem_id last_member_id, ' .
					'last_mem_name last_member_name, ' .
					'most_date, ' .
					'most_count, ' .
					'mem_count ' .
				'from ibf_stats ' .
				'limit 1';
		
		return $this->getDBHelper()->fetch($query);
	}

}
