<?php
namespace org\bgdev\forums\database;

class TopicsHelper extends AbstractHelper {
	
	private $entry;
	
	public function selectEntry(int $entry): void {
		$this->entry = $entry;
	}

	public function updateViewCounter(int $topic_id): bool {
		$query = 'update ibf_topics set views = views + 1 where tid = :id';
		
		return $this->getDBHelper()->execute($query, [
			'id' => $topic_id
		]);
	}
	
	public function getTopic(int $topic_id): array {
		$query =
				'select ' .
					//'* ' .
					'tid id, forum_id, title, ibf_topics.description, icon_id, state, ibf_topics.posts, views, ' .
					'starter_id, start_date, starter_name, ' .
					'ibf_topics.last_poster_id, ibf_topics.last_post, ibf_topics.last_poster_name, ' .
					'cast(substring_index(moved_to, \'&\', 1) as signed) as moved_to_topic, ' .
					'cast(substring_index(moved_to, \'&\', -1) as signed) as moved_to_forum ' .
				'from ibf_topics ' .
				'join ibf_forums on (ibf_topics.forum_id = ibf_forums.id) ' .
				'where tid = :topic_id ' .
					'and (read_perms = \'*\' or read_perms like :group) ' .
				'limit 1';
		
		return $this->getDBHelper()->fetch($query, [
			'topic_id' => $topic_id,
			'group' => '%' . $this->getSession()->getMemberGroup() . '%'
		]);
	}
	
	public function getTopics(int $forum_id, int $offset, int $limit): array {
		$query =
				'select ' .
					//'* ' .
					'tid id, title, ibf_topics.description, icon_id, state, ibf_topics.posts, views, ' .
					'starter_id, start_date, starter_name, pinned, ' .
					'ibf_topics.last_poster_id, ibf_topics.last_post, ibf_topics.last_poster_name, ' .
					'cast(substring_index(moved_to, \'&\', 1) as signed) as moved_to_topic, ' .
					'cast(substring_index(moved_to, \'&\', -1) as signed) as moved_to_forum ' .
				'from ibf_topics ' .
				'join ibf_forums on (ibf_topics.forum_id = ibf_forums.id) ' .
				'where forum_id = :forum_id ' .
					'and (read_perms = \'*\' or read_perms like :group) ' .
				'order by pinned desc, last_post desc ' .
				'limit :offset, :limit';
		
		return $this->getDBHelper()->fetchAll($query, [
			'forum_id' => $forum_id,
			'group' => '%' . $this->getSession()->getMemberGroup() . '%',
			'offset' => $offset,
			'limit' => $limit
		]);
	}

	public function getPosts(int $topic_id, int $offset, int $limit): array {		
		$query =
				'select ' .
					//'* ' .
					'pid as id, icon_id, post_date, post_title, post, ' .
					'author_id, author_name, edit_time, edit_name, ' .
					'(pid = :entry) selected  ' .
				'from ibf_posts ' .
				'join ibf_forums on (ibf_posts.forum_id = ibf_forums.id) ' .
				'where topic_id = :topic_id ' .
					'and (read_perms = \'*\' or read_perms like :group) ' .
				'order by new_topic desc, post_date asc ' .
				'limit :offset, :limit';
		
		return $this->getDBHelper()->fetchAll($query, [
			'topic_id' =>  $topic_id,
			'entry' => $this->entry,
			'group' => '%' . $this->getSession()->getMemberGroup() . '%',
			'offset' => $offset,
			'limit' => $limit
		]);
	}
}
