<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\Response;
use \org\bgdev\forums\database\DBHelper;
use \org\bgdev\forums\database\SessionHelper;

use \org\bgdev\forums\filters\Filter;

abstract class AbstractEndpoint implements Endpoint {
	
	private $args;

	private $helper;
	private $session;

	private $response_code;
	
	private $filters = [];

	public function __construct(array $args) {
		$this->args = $args;
		$this->response_code = 200;
	}
	
	public function getDBHelper(): DBHelper {
		return $this->helper;
	}
	
	public function setDBHelper(DBHelper $helper): void {
		$this->helper = $helper;
	}
	
	public function getSession(): SessionHelper {
		return $this->session;
	}
	
	public function setSession(SessionHelper $session): void {
		$this->session = $session;
	}

	function addFilter(Filter $filter) {
		$filter->setEndpoint($this);
		$this->filters[] = $filter;
	}
	
	protected function setResponseCode(int $response_code): void {
		$this->response_code = $response_code;
	}

	public function getResponse(): Response {
		foreach($this->filters as $filter) {
			if($filter->test()) {
				return $filter->getResponse();
			}
		}
		
		return new Response($this->getValue(), $this->response_code);
	}
	
	protected function getIntParam(string $key, int $def = 0): int {
		return array_key_exists($key, $this->args) ? (int)$this->args[$key] : $def;
	}
	
	protected function getStringParam(string $key, string $def = ''): string {
		return array_key_exists($key, $this->args) ? (string)$this->args[$key] : $def;
	}

	public abstract function getValue(): array;
}
