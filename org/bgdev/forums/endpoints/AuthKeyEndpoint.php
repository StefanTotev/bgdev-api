<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\AuthKeyHelper;

class AuthKeyEndpoint extends AbstractEndpoint {
	
	public function getValue(): array {
		return (new AuthKeyHelper($this))->getAuthKey();
	}
}
