<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\CategoriesHelper;

class CategoriesEndpoint extends AbstractEndpoint {
	
	public function __construct() {
		parent::__construct([]);
	}
	
	public function getValue(): array {
		$this->getSession()->setCurrentForum(0);
		$this->getSession()->setCurrentTopic(0);
		
		$ch = new CategoriesHelper($this);
		return $ch->getAllCategories();
	}
}
