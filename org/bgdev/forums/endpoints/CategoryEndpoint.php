<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\CategoriesHelper;
use \org\bgdev\forums\database\ForumsHelper;
use \org\bgdev\forums\database\MembersHelper;

class CategoryEndpoint extends AbstractEndpoint {
	private $category_id;
			
	public function __construct(array $args) {
		parent::__construct($args);
		$this->category_id = $this->getIntParam('category_id');
	}
	
	public function getValue(): array {
		$this->getSession()->setCurrentForum(0);
		$this->getSession()->setCurrentTopic(0);
		
		$ch = new CategoriesHelper($this);
		$fh = new ForumsHelper($this);
		$mh = new MembersHelper($this);
		
		return [
			'category' => $ch->getCategory($this->category_id),
			'forums' => $fh->getForums($this->category_id),
			'online' => $mh->getMembersInCategory($this->getSession()->getMemberId(), $this->category_id)
		];
	}
}
