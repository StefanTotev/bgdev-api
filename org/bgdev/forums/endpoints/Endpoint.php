<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\DBHelper;
use \org\bgdev\forums\database\SessionHelper;
use \org\bgdev\forums\Response;

interface Endpoint {
	public function getDBHelper(): DBHelper;
	public function setDBHelper(DBHelper $helper): void;
	
	public function getSession(): SessionHelper;
	public function setSession(SessionHelper $session): void;

	public function getResponse(): Response;
}
