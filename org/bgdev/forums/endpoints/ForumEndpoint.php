<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\ForumsHelper;
use \org\bgdev\forums\database\TopicsHelper;
use \org\bgdev\forums\database\MembersHelper;

class ForumEndpoint extends AbstractEndpoint {
	
	private $forum_id;

	private $offset;
	private $limit;

	public function __construct(array $args) {
		parent::__construct($args);
		$this->forum_id = $this->getIntParam('forum_id');
		
		$this->offset = $this->getIntParam('offset');
		$this->limit = $this->getIntParam('limit', TOPICS_PER_PAGE);
		
		$page = $this->getIntParam('page');
		if($page > 0) {
			$this->offset = ($page - 1) * $this->limit;
		}
	}
	
	public function getValue(): array {
		$this->getSession()->setCurrentForum($this->forum_id);
		$this->getSession()->setCurrentTopic(0);
		
		$fh = new ForumsHelper($this);
		$th = new TopicsHelper($this);
		$mh = new MembersHelper($this);
		
		return [
			'forum' => $fh->getForum($this->forum_id),
			'sub_forums' => $fh->getSubForums($this->forum_id),
			'topics' => $th->getTopics($this->forum_id, $this->offset, $this->limit),
			'online' => $mh->getMembersInForum($this->getSession()->getMemberId(), $this->forum_id)
		];
	}
}
