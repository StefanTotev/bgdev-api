<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\MemberHelper;

class LoginEndpoint extends AbstractEndpoint {
	
	public function getValue(): array {
		$this->setResponseCode(302); // bg dev отговаря с 302 на тая заявка
	
		$mh = new MemberHelper($this);
		
		return $mh->login([
			'username' => $this->getStringParam('UserName'), 
			'password' => $this->getStringParam('PassWord')
		]);
	}
}
