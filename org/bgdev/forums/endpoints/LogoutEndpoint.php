<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\MemberHelper;

class LogoutEndpoint extends AbstractEndpoint {
		
	public function getValue(): array {
		$this->setResponseCode(302); // bg dev отговаря с 302 на тая заявка
		return (new MemberHelper($this))->logout();
	}
}
