<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\MembersHelper;

class MemberEndpoint extends AbstractEndpoint {
	
	private $member_id;
	
	public function __construct(array $args) {
		parent::__construct($args);
		$this->member_id = $this->getIntParam('member_id');
	}

	public function getValue(): array {
		$mh = new MembersHelper($this);
		return $mh->getMemeber($this->member_id);
	}
}
