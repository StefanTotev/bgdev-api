<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\MembersHelper;

class MembersEndpoint extends AbstractEndpoint {
	
	private $offset;
	private $limit;

	public function __construct(array $args) {
		parent::__construct($args);
		
		$this->offset = $this->getIntParam('offset');
		$this->limit = $this->getIntParam('limit', MEMBERS_PER_PAGE);
		
		$page = $this->getIntParam('page');
		if($page > 0) {
			$this->offset = ($page - 1) * $this->limit;
		}
	}
	
	public function getValue(): array {
//		$page = ($page > 0) ? $page - 1 : 0;
		
		$mh = new MembersHelper($this);
		return $mh->getAllMemebers($this->offset, $this->limit);
	}
}
