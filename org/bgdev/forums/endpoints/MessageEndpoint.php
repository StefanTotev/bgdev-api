<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\MessagesHelper;

class MessageEndpoint extends AbstractEndpoint {
	
	private $msg_id;
	
	function __construct(array $args) {
		parent::__construct($args);
		$this->msg_id = $this->getIntParam('msg_id');
	}

	public function getValue(): array {
		$mh = new MessagesHelper($this);
		return $mh->getMessage($this->msg_id);
	}
}
