<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\MessagesHelper;

class MessagesEndpoint extends AbstractEndpoint {
	
	private $offset;
	private $limit;
	
	private $inbox;

	public function __construct(array $args) {
		parent::__construct($args);
		
		$this->offset = $this->getIntParam('offset');
		$this->limit = $this->getIntParam('limit', MESSAGES_PER_PAGE);
		
		$path = strtolower($args[0]);
		$this->inbox = strpos($path, 'inbox') !== false;
		
		$page = $this->getIntParam('page');
		if($page > 0) {
			$this->offset = ($page - 1) * $this->limit;
		}
	}
	
	public function getValue(): array {
		//$page = ($page > 0) ? $page - 1 : 0;
		
		$mh = new MessagesHelper($this);
		
		if($this->inbox) {
			return $mh->getInbox($this->offset, $this->limit);
		}
		
		return $mh->getOutbox($this->offset, $this->limit);
	}
}
