<?php
namespace org\bgdev\forums\endpoints;

class RedirectEndpoint extends AbstractEndpoint {
	
	private $location;
	
	public function setLocation(string $location): void {
		$this->location = $location;
	}

	public function getValue(): array {
		$this->setResponseCode(307);
		
		return [
			'location' => $this->location
		];
	}
}
