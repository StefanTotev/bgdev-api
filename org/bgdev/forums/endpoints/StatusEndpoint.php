<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\StatusHelper;
use \org\bgdev\forums\database\MembersHelper;

class StatusEndpoint extends AbstractEndpoint {
	
	public function __construct() {
		parent::__construct([]);
	}
	
	public function getValue(): array {
		$sh = new StatusHelper($this);
		$mh = new MembersHelper($this);
		
		return [
			'version' => '1.0.0.0',
			'settings' => [
				'posts_per_page' => POSTS_PER_PAGE,
				'topics_per_page' => TOPICS_PER_PAGE,
				'members_per_page' => MEMBERS_PER_PAGE,
				'messages_per_page' => MESSAGES_PER_PAGE,
			],
			'user' => [
				'member_id' => $this->getSession()->getMemberId(),
				'member_name' => $this->getSession()->getMemberName()
			],
			'status' => $sh->getSystemInfo(),
			'online' => $mh->getMembersOnLine()
		];
	}
}
