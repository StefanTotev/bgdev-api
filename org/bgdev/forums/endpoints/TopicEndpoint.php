<?php
namespace org\bgdev\forums\endpoints;

use \org\bgdev\forums\database\TopicsHelper;
use \org\bgdev\forums\database\MembersHelper;

class TopicEndpoint extends AbstractEndpoint {
	
	private $topic_id;
	
	private $offset;
	private $limit;
	
	public function __construct(array $args) {
		parent::__construct($args);
		$this->topic_id = $this->getIntParam('topic_id');
		
		$this->offset = $this->getIntParam('offset');
		$this->limit = $this->getIntParam('limit', POSTS_PER_PAGE);
		
		$page = $this->getIntParam('page');
		if($page > 0) {
			$this->offset = ($page - 1) * $this->limit;
		}
	}

	public function getValue(): array {
		$th = new TopicsHelper($this);
		$th->selectEntry($this->getIntParam('entry'));
		
		$id = $this->getSession()->getCurrentTopic();
		if($id !== $this->topic_id) {
			$th->updateViewCounter($this->topic_id);
		}
		
		$topic = $th->getTopic($this->topic_id);
		$forum_id = array_key_exists('forum_id', $topic) ? $topic['forum_id'] : 0;
		
		$this->getSession()->setCurrentForum($forum_id);
		$this->getSession()->setCurrentTopic($this->topic_id);
		
		$mh = new MembersHelper($this);
		
		return [
			'topic' => $topic,
			'posts' => $th->getPosts($this->topic_id, $this->offset, $this->limit),
			'online' => $mh->getMembersInTopic($this->getSession()->getMemberId(), $this->topic_id)
		];
	}
}
