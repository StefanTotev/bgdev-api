<?php
namespace org\bgdev\forums\filters;

use \org\bgdev\forums\endpoints\Endpoint;

abstract class AbstractFilter implements Filter {

	protected $endpoint;
	
	public function setEndpoint(Endpoint $endpoint) {
		$this->endpoint = $endpoint;
	}

	public function test(): bool {
		if($this->endpoint == null) {
			return false;
		}
		
		return $this->filter();
	}
	
	abstract protected function filter(): bool;
}
