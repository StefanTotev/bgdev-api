<?php
namespace org\bgdev\forums\filters;

use \org\bgdev\forums\endpoints\Endpoint;
use \org\bgdev\forums\Response;

interface Filter {
	public function test(): bool;
	
	public function setEndpoint(Endpoint $endpoint);
	public function getResponse(): Response;
}
