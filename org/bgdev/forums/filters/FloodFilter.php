<?php
namespace org\bgdev\forums\filters;

use \org\bgdev\forums\database\MemberHelper;
use \org\bgdev\forums\Response;

class FloodFilter extends AbstractFilter {
	
	protected function filter(): bool {
		$mh = new MemberHelper($this->endpoint);
		return $mh->isFlood();
	}
	
	public function getResponse(): Response {
		return new Response(['message' => 'Too Many Requests'], 429);
	}
}
